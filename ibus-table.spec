Name:           ibus-table
Version:        1.17.8
Release:        1
Summary:        Table engine for Intelligent Input Bus (IBus)
License:        LGPLv2+
URL:            https://github.com/kaio/ibus-table
Source0:        %{url}/releases/download/%{version}/%{name}-%{version}.tar.gz
BuildArch:      noarch

BuildRequires:  ibus-devel > 1.3.0, python3-devel, libappstream-glib
BuildRequires:  gcc docbook-utils appstream desktop-file-utils python3-gobject
BuildRequires:  python3-gobject-base dbus-x11 xorg-x11-server-Xvfb make
BuildRequires:  ibus-table-chinese-wubi-jidian ibus-table-chinese-cangjie ibus-table-chinese-stroke5
Requires:       ibus > 1.3.0, python3 >= 3.3
Obsoletes:      ibus-table-additional < 1.2.0.20100111-5

%description
This module is table engine for Intelligent Input Bus (IBus),
which is an input method framework for multilingual input
in Unix-like operating-systems.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}, pkgconfig

%description    devel
This package includes development files for %{name}.

%package        help
Summary:        man files for %{name}
Requires:       man

%description    help
This package includes man files for %{name}.

%prep
%autosetup -p1

%build
export PYTHON=%{__python3}
%configure --disable-static --disable-additional
%make_build

%install
export PYTHON=%{__python3}
%make_install NO_INDEX=true pkgconfigdir=%{_datadir}/pkgconfig
%py_byte_compile %{__python3} %{buildroot}/usr/share/ibus-table/engine
%py_byte_compile %{__python3} %{buildroot}/usr/share/ibus-table/setup

%find_lang %{name}

%check
appstream-util validate-relax --nonet $RPM_BUILD_ROOT/%{_datadir}/metainfo/*.appdata.xml
desktop-file-validate $RPM_BUILD_ROOT%{_datadir}/applications/ibus-setup-table.desktop
cd engine
python3 table.py
python3 it_util.py
cd -
install -d /tmp/glib-2.0/schemas/
cp org.freedesktop.ibus.engine.table.gschema.xml /tmp/glib-2.0/schemas/
glib-compile-schemas /tmp/glib-2.0/schemas
export XDG_DATA_DIRS=/tmp
eval $(dbus-launch --sh-syntax)
dconf dump /
dconf write /org/freedesktop/ibus/engine/table/wubi-jidian/chinesemode 1
dconf write /org/freedesktop/ibus/engine/table/wubi-jidian/spacekeybehavior false
dconf dump /
ibus-daemon -drx
make check || cat ./tests/test-suite.log

%post
[ -x %{_bindir}/ibus ] && %{_bindir}/ibus write-cache --system >/dev/null 2>&1 || :

%postun
[ -x %{_bindir}/ibus ] && %{_bindir}/ibus write-cache --system >/dev/null 2>&1 || :

%files -f %{name}.lang
%doc AUTHORS COPYING README
%{_datadir}/%{name}
%{_datadir}/metainfo/*.appdata.xml
%{_datadir}/ibus/component/table.xml
%{_datadir}/applications/*.desktop
%{_datadir}/glib-2.0/schemas/org.freedesktop.*.gschema.xml
%{_bindir}/*-createdb
%{_libexecdir}/ibus-*-table
%{_datadir}/icons/hicolor/16x16/apps/ibus-table.png
%{_datadir}/icons/hicolor/22x22/apps/ibus-table.png
%{_datadir}/icons/hicolor/32x32/apps/ibus-table.png
%{_datadir}/icons/hicolor/48x48/apps/ibus-table.png
%{_datadir}/icons/hicolor/64x64/apps/ibus-table.png
%{_datadir}/icons/hicolor/128x128/apps/ibus-table.png
%{_datadir}/icons/hicolor/256x256/apps/ibus-table.png
%{_datadir}/icons/hicolor/scalable/apps/ibus-table.svg


%files devel
%{_datadir}/pkgconfig/*.pc

%files help
%{_mandir}/man*/*

%changelog
* Tue Nov 26 2024 wangxiaomeng <wangxiaomeng@kylinos.cn> - 1.17.8-1
- update to version 1.17.8

* Mon Mar 11 2024 liweigang <liweiganga@uniontech.com> - 1.17.4-1
- update to version 1.17.4

* Fri Sep 01 2023 chenchen <chen_aka_jan@163.com> - 1.17.2-1
- Upgrade to version 1.17.2

* Sat Jan 09 2021 Ge Wang <wangge20@huawei.com> - 1.9.21-6
- Modify homepage url

* Tue Jun 23 2020 lizhenhua <lizhenhua21@huawei.com> - 1.9.21-5
- Add exist_ok=True in os.makedirs to avoid failure due to race condition

* Tue Apr 7 2020 fuanan <fuanan3@huawei.com> - 1.9.21-4
- Replace Source0

* Fri Nov 29 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.9.21-3
- Package init
